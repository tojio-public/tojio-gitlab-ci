#!/bin/bash

echo "CI: Information: Ausgabe deploy.sh"

if [[ -z $CICD_STAGING_KEY ]]; then
  echo "CI: staging key not supplied, please add the secret key of the remote server as gitlab variable named CICD_STAGING_KEY"
  exit 1;
fi
if [[ -z $CICD_SERVER_USER ]]; then
  echo "CI: server user not supplied, please add the server user name of the remote server as gitlab variable named CICD_SERVER_USER"
  exit 1;
fi
if [[ -z $CICD_SERVER ]]; then
  echo "CI: server not supplied, please add the name of the remote server as gitlab variable named CICD_SERVER"
  exit 1;
fi
if [[ -z $CICD_PROJECT_DIR ]]; then
  echo "CI: project directory not supplied, please add the project directory from the remote server as absolute path with no trailing slash as gitlab variable named CICD_PROJECT_DIR"
  exit 1;
fi

# get variables
SERVER_USER=$CICD_SERVER_USER
SERVER=$CICD_SERVER
# absolute path to the project, no trailing slash
PROJECT_DIR=$CICD_PROJECT_DIR
# environemnt name set in .gitlab-ci.yml: dev or live
PROJECT_NAME=$CI_ENVIRONMENT_NAME

# echo out variables
echo "CI: User@server:  " $SERVER_USER@$SERVER 
echo "CI: Project dir:  " $PROJECT_DIR
echo "CI: Project Name: " $PROJECT_NAME

# install and prepare ssh agent
which ssh-agent || ( apt-get update -y && apt-get install openssh-client -y )
mkdir -p ~/.ssh
eval $(ssh-agent -s)
[[ -f /.dockerenv ]] && echo -e "Host *\n\tStrictHostKeyChecking no\n\n" > ~/.ssh/config
ssh-add <(echo "$CICD_STAGING_KEY")

# deployment
echo "CI: start deployment"
echo "CI: create database dump"
echo "\
  cd $PROJECT_DIR/$PROJECT_NAME/web
  mkdir -p -- "../../dbbackups"
  drush status
  drush sql:dump --gzip --result-file=../../dbbackups/${CI_ENVIRONMENT_NAME}_${CI_BUILD_ID}.sql
  mkdir $PROJECT_DIR/_tmp
  " | ssh -p22 -T $SERVER_USER@$SERVER

echo "CI: copy files"
rm -rf artifacts
rm -rf files
rm -rf settings
tar cfz - . | ssh $SERVER_USER@$SERVER "cd $PROJECT_DIR/_tmp; tar xzf -"

echo "CI: copy settings, update db and import configuration"
echo "\
  mv $PROJECT_DIR/$PROJECT_NAME $PROJECT_DIR/old
  mv $PROJECT_DIR/_tmp $PROJECT_DIR/$PROJECT_NAME
  mv $PROJECT_DIR/old/settings $PROJECT_DIR/$PROJECT_NAME/settings
  mv $PROJECT_DIR/old/files $PROJECT_DIR/$PROJECT_NAME/files
  cp $PROJECT_DIR/old/drush/drush.yml $PROJECT_DIR/$PROJECT_NAME/drush/
  rm -rf $PROJECT_DIR/old
  " | ssh -p22 -T $SERVER_USER@$SERVER

echo "\
  cd $PROJECT_DIR/$PROJECT_NAME/web
  drush updb -y
  " | ssh -p22 -T $SERVER_USER@$SERVER
if [ "$?" -ne "0" ]; then
  echo "Update DB with drush failed, exiting."
  exit -1
fi

echo "\
  cd $PROJECT_DIR/$PROJECT_NAME/web
  drush cim -y
  " | ssh -p22 -T $SERVER_USER@$SERVER
if [ "$?" -ne "0" ]; then
  echo "Configuration import with drush failed, exiting."
  exit -1
fi

echo "CI: import translations, rebuild cache"
echo "\
  cd $PROJECT_DIR/$PROJECT_NAME/web
  drush locale-check
  drush locale-update
  drush cr
  " | ssh -p22 -T $SERVER_USER@$SERVER

echo "CI: apply git repository settings"
echo "CI: origin: git@gitlab.com:"$CI_PROJECT_PATH.git
echo "CI: branch: " $BRANCH
echo "\
  cd $PROJECT_DIR/$PROJECT_NAME
  git remote set-url origin git@gitlab.com:$CI_PROJECT_PATH.git
  git checkout $BRANCH
  git pull
  git status
  " | ssh -p22 -T $SERVER_USER@$SERVER
  
echo "CI: finished deployment"
